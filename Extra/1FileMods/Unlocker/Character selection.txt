Race/Class	Archer	WarriorMage	MasterArcher	Sniper	Cleric	Priest	PriestLight	PriestDark	DarkElf	Patriarch	Dragon	GreatWyrm	Druid	GreatDruid	Warlock	ArchDruid	Knight	Cavalier	BlackKnight	Champion	Minotaur	MinotaurLord	Monk	Initiate	Master	Ninja	Paladin	Crusader	Hero	Villain	Ranger	Hunter	BountyHunter	RangerLord	Thief	Rogue	Assassin	Spy	Troll	WarTroll	Vampire	Nosferatu	Sorcerer	Wizard	Necromancer	Lich	ArchMage	Dark ArchMage	Peasant	n/u	High Priest	Master Wizard	n/u
0	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
1	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
2	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
3	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
4	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
5	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
6	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
7	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
8	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
9	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
10	x	-	-	-	x	x	-	-	x	-	x	-	x	-	-	-	x	-	-	-	x	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	-	-	x	-	x	-	x	-	x	-	-	-	x	-	-	-	-
	Continents:
Jadam	Portraits exceptions:																	
	Available classes:	0	4	5	8	10	12	16	20	22	26	30	34	38	40	42	44	48
	Available races:	0	1	2	3	4	5	6	7	8	9	10
Antagarich	Portraits exceptions:																	
	Available classes:	0	4	5	8	10	12	16	20	22	26	30	34	38	40	42	44	48
	Available races:	0	1	2	3	4	5	6	7	8	9	10
Enroth	Portraits exceptions:																	
	Available classes:	0	4	5	8	10	12	16	20	22	26	30	34	38	40	42	44	48
	Available races:	0	1	2	3	4	5	6	7	8	9	10
Between time	Portraits exceptions:																	
	Available classes:	0	4	5	8	10	12	16	20	22	26	30	34	38	40	42	44	48
	Available races:	0	1	2	3	4	5	6	7	8	9	10