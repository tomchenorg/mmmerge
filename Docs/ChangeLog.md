##### 2019-12-15
- Git repository initialization.

##### 2019-12-23
- Ajusted Bolster HP formula (by fanasilver)
- Debug Tent button at Character Creation (by majaczek)
- Force Zombies to be Undead when made through Character Creation in order not to have the Zombie status (by majaczek)
- Racial Skills are automatically learned if a character was created by using the Character Creation screen (by majaczek)
- Better Autobiographies for Characters Created During Character Creation (by majaczek)

##### 2020-01-13
- Restore Data/Tables CRLF behaviour.

##### 2020-01-14
- Restore CRLF behavior of Scripts/ files to Rodril's one.
- Remove accidental CRLF in two Data/Tables files.

##### 2020-01-15
- Made Rodril's Pack of 22.09.2019 and Patch of 8.10.2019 to be ancestors of Community version.

##### 2020-01-21
- Updated version of Zombies to be Undead workaround.

##### 2020-01-23
- Removed accidental characters from Data/Tables.

##### 2020-01-27
- Added support for various RaceNames via Data/Tables/Races.txt.
- Reworked enhanced autobiographies.

##### 2020-01-30
- Added basic Merge Settings support.
- Added various versions: Pack version, Patch version, Community version, SaveGameFormat version.
- Made enhanced autobiographies a toggleable setting.

