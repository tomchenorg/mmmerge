-- Custom Race/Class Names

Log(Merge.Log.Info, "Init started: CustomRaceClassNames.lua")

local function ProcessCustomRaceClassNamesTxt()
	local CustomRaceClassNames = {}

	local TxtTable = io.open("Data/Tables/CustomRaceClassNames.txt", "r")

	if not TxtTable then
		Log(Merge.Log.Warning, "No CustomRaceClassNames.txt found, creating one.")
		TxtTable = io.open("Data/Tables/CustomRaceClassNames.txt", "w")
		TxtTable:write("#\9Continent\9RaceKind\9RaceFamily\9Race\9Alignment\9ClassKind\9ClassStep\9Class\9RaceName\9RacePlural\9RaceAdj\9ClassName\n")
	else
		local LineIt = TxtTable:lines()
		local header = LineIt()
		if header ~= "#\9Continent\9RaceKind\9RaceFamily\9Race\9Alignment\9ClassKind\9ClassStep\9Class\9RaceName\9RacePlural\9RaceAdj\9ClassName" then
			Log(Merge.Log.Error, "CustomRaceClassNames.txt header differs from expected one, table is ignored. Regenerate or fix it.")
		else
			local linenum = 1
			for line in LineIt do
				linenum = linenum + 1
				local Words = string.split(line, "\9")
				if string.len(Words[1]) == 0 then
					Log(Merge.Log.Warning, "CustomRaceClassNames.txt line %d first field is empty. Skipping following lines.", linenum)
					break
				end
				-- Ignore lines that don't start with number
				if Words[1] and tonumber(Words[1]) then
					-- Also ignore lines that have either less than 11 or more than 13 columns
					if #Words > 10 and #Words < 14 then
						local CustomName = {}
						CustomName.ContinentId = tonumber(Words[2])
						CustomName.RaceKindId = tonumber(Words[3])
						CustomName.RaceFamilyId = tonumber(Words[4])
						CustomName.RaceId = tonumber(Words[5])
						CustomName.AlignmentId = tonumber(Words[6])
						CustomName.ClassKindId = tonumber(Words[7])
						CustomName.ClassStepId = tonumber(Words[8])
						CustomName.ClassId = tonumber(Words[9])
						CustomName.RaceName = Words[10]
						CustomName.RacePlural = Words[11]
						CustomName.RaceAdj = Words[12]
						CustomName.ClassName = Words[13]
						table.insert(CustomRaceClassNames, CustomName)
					else
						Log(Merge.Log.Warning, "CustomRaceClassNames.txt line %d has %d columns. Ignoring line.", linenum, #Words)
					end
				else
					Log(Merge.Log.Warning, "CustomRaceClassNames.txt line %d first field is not a number. Ignoring line.", linenum)
				end
			end
		end
	end

	io.close(TxtTable)
	Game.CustomRaceClassNames = CustomRaceClassNames
end

local function EvaluateMacros(Str, Params)
	Str = string.gsub(Str, "{RN}", Params.RaceId ~= -1 and Game.Races[Params.RaceId]
			and Game.Races[Params.RaceId].Name or "")
	Str = string.gsub(Str, "{RP}", Params.RaceId ~= -1 and Game.Races[Params.RaceId]
			and Game.Races[Params.RaceId].Plural or "")
	Str = string.gsub(Str, "{RA}", Params.RaceId ~= -1 and Game.Races[Params.RaceId]
			and Game.Races[Params.RaceId].Adj or "")
	Str = string.gsub(Str, "{BRN}", Params.RaceId ~= -1 and Game.Races[Params.RaceId].BaseRace
			and Game.Races[Game.Races[Params.RaceId].BaseRace]
			and Game.Races[Game.Races[Params.RaceId].BaseRace].Name or "")
	Str = string.gsub(Str, "{BRP}", Params.RaceId ~= -1 and Game.Races[Params.RaceId].BaseRace
			and Game.Races[Game.Races[Params.RaceId].BaseRace]
			and Game.Races[Game.Races[Params.RaceId].BaseRace].Plural or "")
	Str = string.gsub(Str, "{BRA}", Params.RaceId ~= -1 and Game.Races[Params.RaceId].BaseRace
			and Game.Races[Game.Races[Params.RaceId].BaseRace]
			and Game.Races[Game.Races[Params.RaceId].BaseRace].Adj or "")
	Str = string.gsub(Str, "{CN}", Params.ClassId ~= -1
			and Game.ClassNames[Params.ClassId] or "")
	return Str
end

local function CheckRaceId(RaceId)
	if RaceId == nil then
		return false
	elseif not tonumber(RaceId) then
		-- Id should be a number
		return false
	elseif RaceId < 0 or RaceId >= Game.RacesCount then
		-- Assuming we don't have spaces inside Game.Races, given Id is out of bounds
		return false
	elseif Game.Races[RaceId] == nil then
		-- There is no Race structure with given Id
		return false
	else
		return true
	end
end

local function CheckClassId(ClassId)
	if ClassId == nil then
		return false
	elseif not tonumber(ClassId) then
		-- Id should be a number
		return false
	elseif ClassId < 0 or ClassId >= Game.ClassNames.count then
		-- Assuming we don't have spaces inside Game.ClassNames, given Id is out of bounds
		return false
	elseif Game.ClassesExtra[ClassId] == nil then
		-- There is no ClassExtra structure with given Class Id
		return false
	else
		return true
	end
end

function GetRaceName(RaceId, ContinentId, AlignmentId, ClassId, NameType)
	if type(RaceId) == "table" then
		ClassId = RaceId.ClassId
		ContinentId = RaceId.ContinentId or -1
		AlignmentId = RaceId.AlignmentId or -1
		NameType = RaceId.NameType or 0
		RaceId = RaceId.RaceId
	else
		ContinentId = ContinentId or -1
		AlignmentId = AlignmentId or -1
		NameType = NameType or 0
	end
	if not CheckRaceId(RaceId) then
		return nil
	end
	if not CheckClassId(ClassId) then
		return nil
	end
	RaceFamilyId = Game.Races[RaceId].Family or -1
	RaceKindId = Game.Races[RaceId].Kind or -1
	ClassKindId = Game.ClassesExtra[ClassId].Kind or -1
	ClassStepId = Game.ClassesExtra[ClassId].Step or -1
	for _, CustomName in pairs(Game.CustomRaceClassNames) do
		if (CustomName.RaceId == -1 or CustomName.RaceId == RaceId)
				and (CustomName.RaceFamilyId == -1 or CustomName.RaceFamilyId == RaceFamilyId)
				and (CustomName.RaceKindId == -1 or CustomName.RaceKindId == RaceKindId)
				and (CustomName.ContinentId == -1 or CustomName.ContinentId == ContinentId)
				and (CustomName.AlignmentId == -1 or CustomName.AlignmentId == AlignmentId)
				and (CustomName.ClassKindId == -1 or CustomName.ClassKindId == ClassKindId)
				and (CustomName.ClassStepId == -1 or CustomName.ClassStepId == ClassStepId)
				and (CustomName.ClassId == -1 or CustomName.ClassId == ClassId) then
			local RaceName
			if NameType == 0 then
				RaceName = CustomName.RaceName
			elseif NameType == 1 then
				RaceName = CustomName.RacePlural
			elseif NameType == 2 then
				RaceName = CustomName.RaceAdj
			end
			if RaceName == "" or RaceName == nil then
				if NameType == 0 then
					RaceName = Game.Races[CustomName.RaceId].Name
				elseif NameType == 1 then
					RaceName = Game.Races[CustomName.RaceId].Plural
				elseif NameType == 2 then
					RaceName = Game.Races[CustomName.RaceId].Adj
				end
			elseif RaceName == "-" then
				RaceName = ""
			else
				RaceName = EvaluateMacros(RaceName,
						{RaceId = RaceId, RaceFamilyId = RaceFamilyId,
						RaceKindId = RaceKindId, ContinentId = ContinentId,
						AlignmentId = AlignmentId, ClassId = ClassId,
						ClassKindId = ClassKindId, ClassStepId = ClassStepId})
			end
			return RaceName
		end
	end
	-- We haven't found Custom Race Name, return regular one
	if NameType == 0 then
		return Game.Races[RaceId].Name
	elseif NameType == 1 then
		return Game.Races[RaceId].Plural
	elseif NameType == 2 then
		return Game.Races[RaceId].Adj
	end
	-- Name of last resort
	return Game.Races[RaceId].Name
end

function GetClassName(ClassId, ContinentId, RaceId, AlignmentId)
	if type(ClassId) ==  "table" then
		RaceId = ClassId.RaceId
		ContinentId = ClassId.ContinentId or -1
		AlignmentId = ClassId.AlignmentId or -1
		ClassId = ClassId.ClassId
	else
		ContinentId = ContinentId or -1
		AlignmentId = AlignmentId or -1
	end
	if not CheckClassId(ClassId) then
		return nil
	end
	if not CheckRaceId(RaceId) then
		return nil
	end
	ClassKindId = Game.ClassesExtra[ClassId].Kind or -1
	ClassStepId = Game.ClassesExtra[ClassId].Step or -1
	RaceFamilyId = Game.Races[RaceId].Family or -1
	RaceKindId = Game.Races[RaceId].Kind or -1
	for _, CustomName in pairs(Game.CustomRaceClassNames) do
		if (CustomName.ClassId == -1 or CustomName.ClassId == ClassId)
				and (CustomName.ClassKindId == -1 or CustomName.ClassKindId == ClassKindId)
				and (CustomName.ClassStepId == -1 or CustomName.ClassStepId == ClassStepId)
				and (CustomName.ContinentId == -1 or CustomName.ContinentId == ContinentId)
				and (CustomName.RaceId == -1 or CustomName.RaceId == RaceId)
				and (CustomName.RaceFamilyId == -1 or CustomName.RaceFamilyId == RaceFamilyId)
				and (CustomName.RaceKindId == -1 or CustomName.RaceKindId == RaceKindId)
				and (CustomName.AlignmentId == -1 or CustomName.AlignmentId == AlignmentId) then
			local ClassName = CustomName.ClassName
			if ClassName == "" or ClassName == nil then
				ClassName = Game.ClassNames[CustomName.ClassId]
			elseif ClassName == "-" then
				ClassName = ""
			else
				ClassName = EvaluateMacros(ClassName,
						{ClassId = ClassId, ClassKindId = ClassKindId,
						ClassStepId = ClassStepId, ContinentId = ContinentId,
						RaceId = RaceId, RaceFamilyId = RaceFamilyId,
						RaceKindId = RaceKindId, AlignmentId = AlignmentId})
			end
			return ClassName
		end
	end
	-- We haven't found Custom Class Name, return regular one
	return Game.ClassNames[ClassId]
end

function events.GameInitialized2()
	ProcessCustomRaceClassNamesTxt()
end

Log(Merge.Log.Info, "Init finished: CustomRaceClassNames.lua")
