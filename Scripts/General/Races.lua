-- Race-related stuff except for const.Race

const.RaceKinds = {
	Human = 0,
	Undead = 1,
	Elf = 2,
	Minotaur = 3,
	Troll = 4,
	Dragon = 5,
	Goblin = 6,
	Dwarf = 7
}

-- Get Character's Race by his Face
function GetCharRace(Char)
	return Game.CharacterPortraits[Char.Face].Race
end

local function ProcessRacesTxt()
	-- Default Race Names*, can be overridden/localized in Data/Tables/Races.txt
	local TXTRaceNames = {
		Human	 = "Human",
		Vampire	 = "Vampire",
		DarkElf	 = "Dark Elf",
		Minotaur = "Minotaur",
		Troll	 = "Troll",
		Dragon	 = "Dragon",
		Undead	 = "Undead",
		Elf	 = "Elf",
		Goblin	 = "Goblin",
		Dwarf	 = "Dwarf",
		Zombie	 = "Zombie"
	}

	local TXTRaceNamesPlural = {
		Human	 = "Humans",
		Vampire	 = "Vampires",
		DarkElf	 = "Dark Elves",
		Minotaur = "Minotaurs",
		Troll	 = "Trolls",
		Dragon	 = "Dragons",
		Undead	 = "Undead",
		Elf	 = "Elves",
		Goblin	 = "Goblins",
		Dwarf	 = "Dwarves",
		Zombie	 = "Zombies"
	}

	local TXTRaceNamesAdj = {
		Human	 = "Human",
		Vampire	 = "Vampire",
		DarkElf	 = "Dark Elven",
		Minotaur = "Minotaur",
		Troll	 = "Troll",
		Dragon	 = "Dragon",
		Undead	 = "Undead",
		Elf	 = "Elven",
		Goblin	 = "Goblin",
		Dwarf	 = "Dwarven",
		Zombie	 = "Zombie"
	}

	local RaceKindsCount = 0

	for k, v in pairs(const.RaceKinds) do
		RaceKindsCount = RaceKindsCount + 1
	end

	Game.RaceKindsCount = RaceKindsCount

	local Races = {}
	local RacesCount = 0

	-- Set Race Names to const.Race key by default
	for k,v in pairs(const.Race) do
		Races[v] = {}
		Races[v].Id = v
		Races[v].StringId = k
		Races[v].Name = TXTRaceNames[k] or k
		Races[v].Plural = TXTRaceNamesPlural[k] or k
		Races[v].Adj = TXTRaceNamesAdj[k] or k
		RacesCount = RacesCount + 1
	end

	Game.RacesCount = RacesCount

	-- Set up Race Kinds
	Races[const.Race.Human].Kind = const.RaceKinds.Human
	Races[const.Race.Vampire].Kind = const.RaceKinds.Undead
	Races[const.Race.DarkElf].Kind = const.RaceKinds.Elf
	Races[const.Race.Minotaur].Kind = const.RaceKinds.Minotaur
	Races[const.Race.Troll].Kind = const.RaceKinds.Troll
	Races[const.Race.Dragon].Kind = const.RaceKinds.Dragon
	Races[const.Race.Undead].Kind = const.RaceKinds.Undead
	Races[const.Race.Elf].Kind = const.RaceKinds.Elf
	Races[const.Race.Goblin].Kind = const.RaceKinds.Goblin
	Races[const.Race.Dwarf].Kind = const.RaceKinds.Dwarf
	Races[const.Race.Zombie].Kind = const.RaceKinds.Undead

	local TxtTable = io.open("Data/Tables/Races.txt", "r")

	if not TxtTable then
		TxtTable = io.open("Data/Tables/Races.txt", "w")
		TxtTable:write("#\9StringId\9Kind\9Name\9Plural\9Adjective\n")
		for k, v in pairs(Races) do
			TxtTable:write(string.format("%d\9%s\9%d\9%s\9%s\9%s\n", k, v.StringId, v.Kind, v.Name, v.Plural, v.Adj))
		end
	else
		local LineIt = TxtTable:lines()
		LineIt() -- skip header

		for line in LineIt do
			local Words = string.split(line, "\9")
			if string.len(Words[1]) == 0 then
				break
			end
			if Words[1] and tonumber(Words[1]) then
				local race = tonumber(Words[1])
				-- We won't take StringId from TxtTable so skip Words[2]
				local racekind = tonumber(Words[3])
				if racekind and racekind >= 0 and racekind <= Game.RaceKindsCount then
					Races[race].Kind = racekind
				end
				Races[race].Name = Words[4] or Races[race].Name
				Races[race].Plural = Words[5] or Races[race].Name
				Races[race].Adj = Words[6] or Races[race].Adj
			end
		end
	end

	io.close(TxtTable)

	-- MM7 MMExtension has Game.Races which isn't present in
	--   MM8/Merge MMExtension, so using this name should be safe
	Game.Races = Races
end

function events.GameInitialized1()
	ProcessRacesTxt()
end

