-- Merge Settings

-- Load user-defined values
-- Use file MMMergeSettings.lua in Game root directory
function LoadUserMergeSettings ()
	print("Loading user settings from MMMergeSettings.lua")
	local f, errstr = loadfile("MMMergeSettings.lua")
	if f then f() else
		print("Cannot load user settings: " .. errstr)
	end
end

-- Initialize default Merge Settings
local function InitializeMergeSettings()

	-- Initialize tables
	Merge = Merge or {}
	Merge.Settings = {}
	Merge.Settings.Logging = {}
	Merge.Settings.Base = {}
	Merge.Settings.Character = {}
	Merge.Settings.Promotions = {}

	-- Set version
	-- Version of the last Rodril's full pack
	Merge.PackVersion = Merge.PackVersion or "20190922"
	-- Version of the last Rodril's patch
	Merge.PatchVersion = Merge.PatchVersion or "20191008"
	-- Version of Community patch. Comment out for Base Merge
	Merge.CommunityVersion = Merge.CommunityVersion or "20200202-Comm"
	-- Set version to latest change
	Merge.Version = Merge.CommunityVersion or Merge.PatchVersion or Merge.PackVersion
	-- Full version string
	Merge.VersionFull = (Merge.CommunityVersion and Merge.CommunityVersion .. " based on " or "")
		.. (Merge.PatchVersion and Merge.PatchVersion .. " Patch of " or "")
		.. Merge.PackVersion .. " Pack"
	-- SaveGame compatibility. Raise when SaveGame format is (incompatibly?) changed.
	-- Version format: YYMMDDxx
	-- FIXME: use the earliest possible known date
	Merge.SaveGameFormatVersion = Merge.SaveGameFormatVersion or 19092200

	------------ Logging settings ------------

	-- Log File Name
	-- Note: if you add subdirectory into File Name, you have to create it first
	--Merge.Settings.Logging.LogFile = "Logs/MMMergeLog.txt"
	Merge.Settings.Logging.LogFile = "MMMergeLog.txt"

	-- Number of old Log Files to preserve [default: 2]
	Merge.Settings.Logging.OldLogsCount = 2

	-- Force immediate flush into log file
	--   0 - [default] don't force
	--   1 - force
	Merge.Settings.Logging.ForceFlush = 0

	-- Print times before log messages
	--   0 - [default] disabled
	--   1 - enabled
	Merge.Settings.Logging.PrintTimes = 0

	-- Print CPU times before log messages also, requires Logging.PrintTimes to be enabled
	--   0 - [default] disabled
	--   1 - enabled
	Merge.Settings.Logging.PrintOsClock = 0

	-- Print message source file before log messages
	--   0 - [default] disabled
	--   1 - enabled
	Merge.Settings.Logging.PrintSources = 0

	-- Print traceback of invalid log message formatting
	--   0 - [default] disable
	--   1 - enable
	Merge.Settings.Logging.PrintFormatTraceback = 0

	-- Log Level
	--    0 - disabled
	--    1 - fatal errors
	--    2 - [default] errors
	--    3 - warnings
	--    4 - informational
	--    5 - debug
	Merge.Settings.Logging.LogLevel = 2

	-- Debug settings (yet to be implemented)
	Merge.Settings.Logging.DebugScope = 0
	Merge.Settings.Logging.DebugFiles = {}

	-------------------------------------------------------
	------------ Default Merge Settings values ------------
	-------------------------------------------------------

	------------ Character settings ------------

	-- Use enhanced autobiographies
	--    0 - [default] use 'Name - Class' style
	--    1 - [community default] use 'Name, the RaceAdj Class' style
	--    2 - use 'Name - Class (Race)' style
	Merge.Settings.Character.EnhancedAutobiographies = 1

	-- Force Zombie character to be of Undead race during Character creation
	--    0 - [default] don't force
	--    1 - [community default] force
	Merge.Settings.Character.ForceZombieToUndeadRace = 1

	-- Autolearn racial skill
	-- Note: this is a hack that is fragile and might cause unexpected behavior
	--   0 - [default] disable
	--   1 - [community default] enable
	Merge.Settings.Character.AutolearnRacialSkillHack = 1

	------------ Promotions settings ------------

	-- Do not convert Character to Undead on Lich Class Promotion
	--    0 - [default] always convert
	--    1 - [community default] do not convert if race is of Undead kind
	--    2 - never convert
	Merge.Settings.Promotions.PreserveRaceOnLichPromotion = 1
end

if not Merge or not Merge.Settings then
	InitializeMergeSettings()
	LoadUserMergeSettings()
end

function events.BeforeSaveGame()
	-- Put SaveGameFormatVersion into the savegame
	vars.SaveGameFormatVersion = Merge.SaveGameFormatVersion
end

