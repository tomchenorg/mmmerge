The Community branch of the MMMerge project - Might and Magic Merge, also known as the World of Enroth.
Check the thread here: https://www.celestialheavens.com/forum/10/16657

# WILL ONLY WORK WITH MERGE VERSION 08.10.2019 !

### Installation instructions -

0. Install the Merge - by installing the original game, then the merge up to the compatible version mentioned (if the newest one by Rodril is newer, please send me an email to templayer@seznam.cz and I will update the community branch) and the newest GrayFace patch on top of that (link is in the first post in the thread)
1. Delete the script folder (we had problems with rogue lua files before...)
2. Copy paste the folders from GitLab into your installation directory, replacing existing (except for the script folder mentioned in the previous point... that one will be added instead of replaced). The download button can be hard to find because it is just an icon without words, so here you go with a link instead: https://gitlab.com/templayer/mmmerge/-/archive/master/mmmerge-master.zip
3. Use and configure any optional content in the "Changes from the Vanilla (Base / non-community branch) Merge" (i.e. following) section
4. Play.

### Changes from the Vanilla (Base / non-community branch) Merge -
1. Icon - there is an icon contained within the branch, made by Templayer out of the Dracolich assets, which were made by GDSpectra, Jamesx and Templayer. Should be compatible with Windows XP, but still includes a 256x256 image variant for systems that support such high-res icons. (also GitLab is telling me that it only contains the 256x256, which is false...)
2. Bolster Monster modifications - Fanasilver made a modification for a less aggressive HP scaling called "Use a non-aggresive formula for HP calculation". Results:

| Bolster | HP coefficient (× Base Health) |
|---------|--------------------------------|
| 0%      | 1                              |
| 100%    | 2                              |
| 150%    | 4                              |
| 200%    | 9                              |

Bolster can be put to original behavior by replacing `Data/Tables/Bolster - formulas.txt` with `Extra/Default/Bolster - formulas.txt`.  
Bolster can be put back to Fanasilver's by replacing `Data/Tables/Bolster - formulas.txt` with `Extra/CommunityDefault/Bolster - formulas.txt`.

3. Debug "tent" button at character creation to tell you the indexes of your choices (currently non-toggleable) by majaczek
4. Zombies will become Undead when made through Character Creation in order not to have the Zombie status (toggleable) by majaczek
5. Racial Skills are automatically learned if a character was created by using the Character Creation screen (toggleable) by majaczek
6. Better Autobiographies for Characters Created During Character Creation by adding Race (toggleable) by majaczek
7. Druid Summoning ALPHA - By holding CTRL while casting a targetting Earth spell by a Druid, summons creatures instead. I won't give you a list of creatures yet, since those are probably going change. By wuxiangjinxing
8. UI Crash Hotfix by CkNoSFeRaTU
9. Race Kinds and extended Race Names support, Game.Races structure and Races.txt table by cthscr (Issue #1)
10. User settings in `MMMergeSettings.lua` file - see [PlayerManual/Configuration](https://gitlab.com/templayer/mmmerge/-/wikis/Docs/PlayerManual/Configuration) for details.
11. Check the Extra folder for different sets of table settings. Currently we have Default (close to Rodril's) and CommunityDefault.
12. Check the Extra/1FileMod folder for small one-file optional table settings (like the Character Creation Unlocker or Racial Skills Alternate table). To use them, replace the existing in Data/Tables folder with whatever you choose.

### TODOs -
1. Make the custom optional modifications toggleable.
List of things to make toggleable - Merge Request 2 (debug "Tent" button), Merge Request 9 - Druid Summoning 
2. Implement stuff on the Implementation Queue on the Merge Tracker - https://goo.gl/ui24Bz
3. Finish Up Druid Summoning - Summoner.lua
4. Replace UI Crash Hotfix with a proper solution (probably one made by Rodril or GrayFace) when it comes out

### Developer notes-
1. For some basic code readability instructions (when writing your own code for the Merge), check the Snippets. There is currently only one, for Magical Numbers.

### Community Branch Credits -
Maintenance: Templayer<br>
Coding: majaczek, fanasilver, CkNoSFeRaTU, wuxiangjinxing, cthscr

